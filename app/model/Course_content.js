const mongoose = require('../libs/mongoose');
const Schema = mongoose.Schema;

const courseContentSchema = new Schema({
    type: String,
    order: Number,
    content: Schema.Types.Mixed
})
exports.CourseContent = mongoose.model('CourseContent', courseContentSchema)