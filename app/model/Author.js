const mongoose = require('../libs/mongoose');
const Schema = mongoose.Schema;

const authorSchema = new Schema({
    name: String,
    logo: String,
})
exports.Author = mongoose.model('Author', authorSchema)