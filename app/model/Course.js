const mongoose = require('../libs/mongoose');
const Schema = mongoose.Schema;

const courseSchema = new Schema({
    name: String,
    logo: String,
    shortDescription: String,
    about: String,
    resourceLink: String,
    authors: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Author"
        }
    ],
    content: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "CourseContent"
        }
    ]
})

exports.Course = mongoose.model('Course', courseSchema)