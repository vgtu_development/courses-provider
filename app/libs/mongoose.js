const mongoose = require("mongoose");
const yaml_config = require('node-yaml-config');
const db_config = yaml_config.load('config/config.yaml').database;

mongoose.connect(buildConnectionString())

/**
 * Method, helps build connections string for mongodb
 * @returns {string} - mongodb connection string
 */
function buildConnectionString() {
    let url = 'mongodb://';
    if (db_config.username && db_config.password) {
        url = url + `${db_config.username}:${db_config.password}@`
    }
    url = url + `${db_config.host}:${db_config.port}`
    if (db_config.authSource) {
        url = url + `/?authSource=${db_config.authSource}`
    }
    return url;
}

module.exports = mongoose;