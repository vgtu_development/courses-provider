const CoursesContentService = require('../services/courses_content_service');

class CoursesController {

    /**
     * Method for handling response codes and messages in case of getting content of course
     * @param req - incoming request
     * @param res - outcoming response
     * @returns {this} - outcoming response
     */
    getContentByCourseId(req, res) {
        const id = req.params.id;
        if (!id) {
            return res.status(400).send({message: 'Course id is empty.'});
        }
        CoursesContentService.getContentByCourseId(id, req.query.start, req.query.limit)
            .then(content => {
                if (content) {
                    return res.status(200).send(content);
                } else {
                    return res.status(404).send({message: `Course content not found in database`});
                }
            })
            .catch(err => res.status(500).send(err));
    }
}

module.exports = new CoursesController();