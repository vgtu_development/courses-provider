const CoursesService = require('../services/courses_service');

class CoursesController {
    /**
     * Method for handling response codes in case of getting list of courses
     * @param req - incoming request
     * @param res - outcoming response
     */
    getCoursesList(req, res) {
        CoursesService.getCoursesList(req.query.start, req.query.limit)
            .then(courses => res.status(200).send(courses))
            .catch(err => res.status(500).send(err));
    }

    /**
     * Method for handling response codes and error messages in case of getting course by id
     * @param req - incoming request
     * @param res - outcoming response
     * @returns {this} - outcoming response
     */
    getCourseById(req, res) {
        const id = req.params.id;
        if (id) {
            CoursesService.getById(id)
                .then(course => {
                    if (course) {
                        return res.status(200).send(course);
                    } else {
                        return res.status(404).send({message: `Course with id ${id} not found in database`});
                    }
                })
                .catch(err => res.status(500).send(err));
        } else {
            return res.status(400).send({message: 'Course id is empty.'});
        }
    }
}

module.exports = new CoursesController();