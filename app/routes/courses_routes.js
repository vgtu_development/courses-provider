const express = require("express"),
    router = express.Router(),
    CoursesController = require('../controllers/courses_controller');

router.route('/list')
    .get(CoursesController.getCoursesList)
router.route('/:id')
    .get(CoursesController.getCourseById)

module.exports = router;