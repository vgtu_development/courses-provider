const express = require("express"),
    router = express.Router(),
    CoursesContentController = require('../controllers/courses_content_controller');

router.route('/:id/content')
    .get(CoursesContentController.getContentByCourseId)

module.exports = router;