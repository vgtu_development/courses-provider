const express = require("express"),
    router = express.Router(),
    coursesRoutes = require("./courses_routes"),
    coursesContentRoutes = require("./courses_content_routes")

router.use('/courses', coursesRoutes);
router.use('/courses', coursesContentRoutes);

module.exports = router;