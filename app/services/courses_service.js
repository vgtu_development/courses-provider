const {Course/*, Author, CourseContent*/} = require('../model/index');

class CoursesService {
    /**
     * Method of finding course by it's id in database
     * @param id - Id of course
     * @returns {Promise<Course>} - Promise object, awaiting which contains course and their authors
     */
    async getById(id) {
        if (!id) {
            return undefined;
        }
        return Course.findOne({_id: id}, (err, result) => {
            if (err) throw err
            return result;
        }).populate('authors');
    }

    /**
     * Method of finding a list of courses from a database
     * @param start - offset from the start. Used for pagination
     * @param limit - maximum number of courses. Used for pagination
     * @returns {Promise} Promise object, awaiting which contains a list of courses
     */
    async getCoursesList(start = 0, limit = Number.MAX_SAFE_INTEGER) {
        // await this.generateTestData();
        return Course.find(undefined, ['_id', 'name', 'logo', 'shortDescription'], {skip: start, limit: limit},
            (err, result) => {
                if (err) throw err
                return result;
            }
        );
    }

    // uncomment this block of code, uncomment `await generateTestData();`, uncomment `/*, Author, CourseContent */` and use /courses/list to insert test data to DB
    /**
     * Special method for generation test data. Commented by default.
     * @returns {Promise<void>}
     */
    /*async generateTestData() {
        const a = new Author({
            name: 'Иванов Иван Иванович',
            logo: 'advbdvsb'
        })
        const a2 = new Author({
            name: 'Петров Петр Петрович',
            logo: 'advbdvsb'
        })
        const c = new Course({
            "name": "Обучение Python с нуля",
            "logo": "http://mypic.ru/bfsnbnsdvnsdkt2d",
            "shortDescription": "<p>Лучший бесплатный онлайн-курс размещенный на платформе</p>",

            "about":    "<p>Курс&nbsp;\"Python для начинающих\" рассчитан на школьников и всех желающих познакомиться с программированием.&nbsp;</p>\n" +
                        "\n" +
                        "<p>В курсе рассматриваются основные типы данных, конструкции и принципы структурного программирования. Используется версия языка Python ветки 3.x.</p>\n" +
                        "\n" +
                        "<p>Выбор Python обусловлен такими его преимуществами как ясность кода и быстрота реализации на нем программ. Основной целью курса является знакомство с программированием, формирование базовых понятий структурного программирования.</p>\n" +
                        "\n" +
                        "<p>Курс разбит на 8&nbsp;модулей, каждый из них содержит теоретические и практические материалы и задания.</p>\n" +
                        "\n" +
                        "<p><strong>Модули курса:</strong></p>\n" +
                        "\n" +
                        "<ol>\n" +
                        "\t<li>Ввод-вывод данных;</li>\n" +
                        "\t<li>Условный оператор;</li>\n" +
                        "\t<li>Типы данных;</li>\n" +
                        "\t<li>Циклы for и while;</li>\n" +
                        "\t<li>Строковый тип данных;</li>\n" +
                        "\t<li>Списки;</li>\n" +
                        "\t<li>Функции;</li>\n" +
                        "\t<li>Работа над мини-проектом.</li>\n" +
                        "</ol>",

            "resourceLink": "https://drive.google.com/drive/folders/1TWHHpDeHzLcau2N0fVeF9FBXKLVHdqq8?usp=sharing",
            "authors": [a, a2]
        })
        const co1 = new CourseContent({
            "type": "html",
            "order": 1,
            "content": {
                "html": "<h1 style=\"text-align: center;\">Тема урока: ввод-вывод данных</h1>\n" +
                    "\n" +
                    "<ol>\n" +
                    "\t<li>Вывод данных, команда <code>print()</code></li>\n" +
                    "\t<li>Ввод данных, команда <code>input()</code></li>\n" +
                    "\t<li>Решение задач</li>\n" +
                    "</ol>\n" +
                    "\n" +
                    "<p><strong>Аннотация.</strong>&nbsp;Ввод и вывод данных в языке Python. Несложные программы, которые умеют что-то выводить на экран (команда <strong>print</strong>) и считывать информацию с клавиатуры (команда <code>input()</code>).</p>\n" +
                    "\n" +
                    "<h2 style=\"text-align: center;\">Вывод данных, команда print</h2>\n" +
                    "\n" +
                    "<p>Для вывода данных&nbsp;на&nbsp;экран используется команда <code>print()</code>.</p>\n" +
                    "\n" +
                    "<p>Внутри круглых скобок пишем, что хотим вывести на экран. Если это текст, то обязательно указываем его внутри кавычек. Кавычки могут быть одинарными или двойными. Только обязательно ставим одинаковые до и после текста.</p>"
            },
        })
        const co2 = new CourseContent({
            "type": "html",
            "order": 2,
            "content": {
                "html": "<h1>Тема урока: работа с целыми числами&nbsp;&nbsp;</h1>\n" +
                    "\n" +
                    "<ol>\n" +
                    "\t<li>Операция&nbsp;возведения в степень</li>\n" +
                    "\t<li>Операция&nbsp;нахождения остатка</li>\n" +
                    "\t<li>Операция&nbsp;целочисленного деления</li>\n" +
                    "\t<li>Обработка цифр числа</li>\n" +
                    "\t<li>Решение задач</li>\n" +
                    "</ol>\n" +
                    "\n" +
                    "<p><strong style=\"font-size: inherit;\">Аннотация.</strong>&nbsp;Урок посвящен дополнительным операциям&nbsp;при работе с целыми числами.&nbsp;Изучим дополнительные операции, а также научимся обрабатывать цифры целого числа.</p>\n" +
                    "\n" +
                    "<h2 style=\"text-align: center;\">Дополнительные операции</h2>\n" +
                    "\n" +
                    "<p>Мы познакомились&nbsp;с <span><span class=\"katex\"><span class=\"katex-mathml\"><math xmlns=\"http://www.w3.org/1998/Math/MathML\"><semantics><mrow><mn>4</mn></mrow><annotation encoding=\"application/x-tex\">4</annotation></semantics></math></span><span class=\"katex-html\" aria-hidden=\"true\"><span class=\"base\"><span class=\"strut\" style=\"height: 0.64444em; vertical-align: 0em;\"></span><span class=\"mord\">4</span></span></span></span></span> основными математическими операциями в языке Python: сумма <span><span class=\"katex\"><span class=\"katex-mathml\"><math xmlns=\"http://www.w3.org/1998/Math/MathML\"><semantics><mrow><mo stretchy=\"false\">(</mo><mo>+</mo><mo stretchy=\"false\">)</mo></mrow><annotation encoding=\"application/x-tex\">(+)</annotation></semantics></math></span><span class=\"katex-html\" aria-hidden=\"true\"><span class=\"base\"><span class=\"strut\" style=\"height: 1em; vertical-align: -0.25em;\"></span><span class=\"mopen\">(</span><span class=\"mord\">+</span><span class=\"mclose\">)</span></span></span></span></span>, разность <span><span class=\"katex\"><span class=\"katex-mathml\"><math xmlns=\"http://www.w3.org/1998/Math/MathML\"><semantics><mrow><mo stretchy=\"false\">(</mo><mo>−</mo><mo stretchy=\"false\">)</mo></mrow><annotation encoding=\"application/x-tex\">(-)</annotation></semantics></math></span><span class=\"katex-html\" aria-hidden=\"true\"><span class=\"base\"><span class=\"strut\" style=\"height: 1em; vertical-align: -0.25em;\"></span><span class=\"mopen\">(</span><span class=\"mord\">−</span><span class=\"mclose\">)</span></span></span></span></span>, произведение <span><span class=\"katex\"><span class=\"katex-mathml\"><math xmlns=\"http://www.w3.org/1998/Math/MathML\"><semantics><mrow><mo stretchy=\"false\">(</mo><mo>∗</mo><mo stretchy=\"false\">)</mo></mrow><annotation encoding=\"application/x-tex\">(*)</annotation></semantics></math></span><span class=\"katex-html\" aria-hidden=\"true\"><span class=\"base\"><span class=\"strut\" style=\"height: 1em; vertical-align: -0.25em;\"></span><span class=\"mopen\">(</span><span class=\"mord\">∗</span><span class=\"mclose\">)</span></span></span></span></span> и частное <span><span class=\"katex\"><span class=\"katex-mathml\"><math xmlns=\"http://www.w3.org/1998/Math/MathML\"><semantics><mrow><mo stretchy=\"false\">(</mo><mi mathvariant=\"normal\">/</mi><mo stretchy=\"false\">)</mo></mrow><annotation encoding=\"application/x-tex\">(/)</annotation></semantics></math></span><span class=\"katex-html\" aria-hidden=\"true\"><span class=\"base\"><span class=\"strut\" style=\"height: 1em; vertical-align: -0.25em;\"></span><span class=\"mopen\">(</span><span class=\"mord\">/</span><span class=\"mclose\">)</span></span></span></span></span>. Добавив ещё <span><span class=\"katex\"><span class=\"katex-mathml\"><math xmlns=\"http://www.w3.org/1998/Math/MathML\"><semantics><mrow><mn>3</mn></mrow><annotation encoding=\"application/x-tex\">3</annotation></semantics></math></span><span class=\"katex-html\" aria-hidden=\"true\"><span class=\"base\"><span class=\"strut\" style=\"height: 0.64444em; vertical-align: 0em;\"></span><span class=\"mord\">3</span></span></span></span></span> операции, мы получим инструментарий, достаточный для написания <span><span class=\"katex\"><span class=\"katex-mathml\"><math xmlns=\"http://www.w3.org/1998/Math/MathML\"><semantics><mrow><mn>99</mn><mi mathvariant=\"normal\">%</mi></mrow><annotation encoding=\"application/x-tex\">99\\%</annotation></semantics></math></span><span class=\"katex-html\" aria-hidden=\"true\"><span class=\"base\"><span class=\"strut\" style=\"height: 0.80556em; vertical-align: -0.05556em;\"></span><span class=\"mord\">9</span><span class=\"mord\">9</span><span class=\"mord\">%</span></span></span></span></span> программ.</p>\n" +
                    "\n" +
                    "<table align=\"center\" border=\"1\" cellpadding=\"1\" cellspacing=\"1\">\n" +
                    "\t<tbody>\n" +
                    "\t\t<tr>\n" +
                    "\t\t\t<td style=\"text-align: center;\"><strong>Оператор</strong></td>\n" +
                    "\t\t\t<td style=\"text-align: center;\"><strong>Описание</strong></td>\n" +
                    "\t\t</tr>\n" +
                    "\t\t<tr>\n" +
                    "\t\t\t<td style=\"text-align: center;\">**</td>\n" +
                    "\t\t\t<td style=\"text-align: center;\">Возведение в степень</td>\n" +
                    "\t\t</tr>\n" +
                    "\t\t<tr>\n" +
                    "\t\t\t<td style=\"text-align: center;\">%</td>\n" +
                    "\t\t\t<td style=\"text-align: center;\">Остаток от деления</td>\n" +
                    "\t\t</tr>\n" +
                    "\t\t<tr>\n" +
                    "\t\t\t<td style=\"text-align: center;\">//</td>\n" +
                    "\t\t\t<td style=\"text-align: center;\">Целочисленное деление</td>\n" +
                    "\t\t</tr>\n" +
                    "\t</tbody>\n" +
                    "</table>"
            },
        })
        const co3 = new CourseContent({
            "type": "video",
            "order": 3,
            "content": {
                "url": "https://www.youtube.com/embed/3wBteulZaAs"
            },
        })
        const final = new CourseContent({
            "type": "final",
            "order": 4,
            "content": {
                "html": "<p>Спасибо за внимание</p>"
            },
        })
        co1.save()
        co2.save()
        co3.save()
        final.save();
        a.save()
        a2.save()
        c.content = [co1, co2, co3, final]
        c.save()
    }*/
}

module.exports = new CoursesService();