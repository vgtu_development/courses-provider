const { Course, CourseContent} = require('../model/index');

class CoursesContentService {

    /**
     *
     * @param id - Id of course
     * @param start - offset from the start. Used for pagination
     * @param limit - maximum number of courses. Used for pagination
     * @returns {Promise<{total: number, content: *}>} - Promise object, awaiting which contains a content of course and total count of content.
     */
    async getContentByCourseId(id, start = 0, limit = Number.MAX_SAFE_INTEGER) {
        if (!id) {
            return undefined;
        }
        const intStart = Number(start);
        const intLimit = Number(limit);
        const content = (await Course.findById(id, (err, result) => {
            if (err) throw err
            if (result) {
                return result;
            }
        }).populate('content')).content
        return {
            content: content.sort((a, b) => (a.order > b.order) ? 1 : -1)
                .slice(intStart, intStart + intLimit),
            total: content.length
        }
    }

}

module.exports = new CoursesContentService();