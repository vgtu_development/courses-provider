# **courses-provider**

This microservice implements the logic of working with courses for students

required:
1. mongoDB is UP
2. configs to mongoDB in `config.yaml`
3. in MongoDB exists collections of cources and authors

Quick start:

0. install `Node.js + npm` last stable version
1. in courses-provider package: `npm i`
2. install mongo in docker:
   1) download and install `docker`: https://hub.docker.com/editions/community/docker-ce-desktop-windows/
   2) in console: `docker run -d -p 127.0.0.1:27017:27017 --name mongo-exp-project mongo`
3. download and install `Robo3t`: https://robomongo.org/download
4. create collections and add documents using commented code in courses_service.js (see comment in this file)
5. in courses-provider package: `node index.js` or `npm start`

Api:

| Request  | Response |
| ------------- | ------------- |
| GET: `/courses/list` optional params: `start`, `limit` | [<br>  {<br>    "id": "1253",<br>    "name": "курс 1",<br>	"logo": "http://mypic.ru/bfsnbnsdvnsdkt2d",<br>	"shortDescription": "Descr"<br>  },<br>  {<br>    "id": "153",<br>    "name": "курс 2",<br>	"logo": "http://mypic.ru/bfsnbghsgd4gfd",<br>	"shortDescription": "Descr2"<br>  },<br>  {<br>    "id": "123",<br>    "name": "курс 3",<br>	"logo": "http://mypic.ru/bfsnbnsdvnsdkt7d",<br>	"shortDescription": "Descr3"<br>  }<br>] |
| GET: `/courses/${id}` required params: `id`  | {<br/>    "id": "1253",<br/>    "name": "курс 1",<br/>	"logo": "http://mypic.ru/bfsnbnsdvnsdkt2d", <br/>	"shortDescription": "Descr",<br/>	"about": "vbsjkfvsdjvbsldvbljsdljbv",<br/>	"resourceLink": "google.doc",<br/>	"authors": [<br/>	  {<br/>	    "authorName": "qwe",<br/>	    "authorLogo": "http://mypic.ru/bfsnbnsdvnsdkt2te",<br/>	  }<br/>	]<br/>}  |
| GET: `/courses/${id}/content` params: required: `id`; optional: `start`, `limit` | {<br/>   "content":[<br/>      {<br/>         "_id":"5ff8cf847d76ec19f4f85efb",<br/>         "type":"html",<br/>         "order":1,<br/>         "content":{<br/>            "html":"<p>Тестовая страничка 1</p>"<br/>         },<br/>         "__v":0<br/>      },<br/>      {<br/>         "_id":"5ff8cf847d76ec19f4f85efc",<br/>         "type":"html",<br/>         "order":2,<br/>         "content":{<br/>            "html":"<p>Тестовая страничка 2</p>"<br/>         },<br/>         "__v":0<br/>      },<br/>      {<br/>         "_id":"5ff8cf847d76ec19f4f85efd",<br/>         "type":"video",<br/>         "order":3,<br/>         "content":{<br/>            "url":"https://www.youtube.com/embed/3wBteulZaAs"<br/>         },<br/>         "__v":0<br/>      },<br/>      {<br/>         "_id":"5ff8cf847d76ec19f4f85efe",<br/>         "type":"final",<br/>         "order":4,<br/>         "content":{<br/>            "html":"<p>Спасибо за внимание</p>"<br/>         },<br/>         "__v":0<br/>      }<br/>   ],<br/>   "total":4<br/>} |