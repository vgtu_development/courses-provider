const express = require('express'),
    app = express(),
    bodyParser = require('body-parser'),
    routes = require('./app/routes/index');

const yaml_config = require('node-yaml-config');
const config = yaml_config.load('config/config.yaml');
const server = config.server;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

app.use('/', routes);

app.listen(server.port, server.host, () => console.log(`Server listens http://${server.host}:${server.port}`));